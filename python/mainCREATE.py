import json
import socket
import sys
import math

class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.angle = 0
        self.d_angle = 0
        self.dd_angle = 0
        self.ddd_angle = 0
        self.ppos = 0
        self.speed = 0
        self.max_speed = 0
        self.accel = 0
        self.lane = None
        self.p_ind = None
        self.num_enemies = 0
        self.tick = 0
        
        self.boosted = False
        self.gotTurbo = False
        self.turboParams = {}
        self.canBoostSafely = False
        self.enemy = {}
        self.waRoom = [] # waiting room, poczekalnia

    def init_external_intelligence_module(self, data):
        # data is from GAMEINIT message
        for x in data['race']['cars']:
            car_id = tuple(sorted(x['id'].items()))
            if car_id != self.car:                      
                self.enemy[car_id] = {}
                self.enemy[car_id][0] = 0
                self.enemy[car_id][1] = 0
                self.num_enemies += 1
        
        

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        #return self.msg("join", {"name": self.name, "key": self.key})
##        return self.msg("joinRace", {"botId": {"name": self.name,
##                                 "key": self.key},
##                  #               "trackName": 'keimola',
##                                 "trackName": 'germany',
##                  #               "trackName": 'usa',
##                  #               "trackName": 'france',
##                                 "carCount": 1})
      return self.msg("createRace", {"botId": {"name": self.name,
                                 "key": self.key},
                  #               "trackName": 'keimola',
                                 "trackName": 'germany',
                  #               "trackName": 'usa',
                  #               "trackName": 'france',
                                 "password": "karamelki",
                                 "carCount": 2})
    
    def switch_lane(self, direction):
        self.lane += 1 if direction == 'Right' else -1
        return self.msg("switchLane", direction)

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def turbo(self):
        #if self.gotTurbo:
        self.msg("turbo", 'AAA!')
        self.boosted = True
        self.gotTurbo = False
        
    def on_turbo_available(self, data):
        self.gotTurbo = True
        self.turboParams = data
        
    def on_turbo_end(self, data):
        print("Boost ended")
        self.boosted = False
        
    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")

    def on_game_start(self, data):
        self.tick = 0
        print("Race started")

    def lane_radius(self, turn, lane):
        for l in self.data_init['race']['track']['lanes']:
            if l['index'] == lane:
                break
        return turn['radius'] - math.copysign(1, turn['angle'])*l['distanceFromCenter']

    def piece_len(self, piece, lane):
        # It looks like {}.get(), but it doesn't compute the default value until it's necessary.
        return piece['length'] if 'length' in piece else math.radians(abs(piece['angle']))*self.lane_radius(piece, lane)

    def same_direction(self, a, b):
        '''Checks if turns a and b turn the same way.
            Returns false if one of the turns is in fact a straight.'''
        n = ('angle' in a) + ('angle' in b)
        if not n:
            return True
        if n == 1:
            return False
        return math.copysign(4, a['angle']) == math.copysign(4, b['angle'])

    def estimate_entry_speed(self, turn, lane):
 # france -12 20
##        X = -12
##        Y = 20
        # usa 120 40
##        X = 120
##        Y = 40
        # germany -20 18 | -10 20
##        X = -10
##        Y = 20
        # finland -10 20
##        X = -12
##        Y = 20
        # safe
##        X = -10
##        Y = 24
##        return (60+X+(self.lane_radius(turn, lane)))/Y
        return math.sqrt(.5*(self.lane_radius(turn, lane)))

    def next_piece_of_concern(self, current_piece_ind, current_piece_dist, lane):
        '''Returns the distance to the piece we should consider next
            as well as estimated speed we shouldn't exceed while approaching it.

            WARNING: Don't call until self.accel is known!'''
        r_dist = 0
        r_speed = 0xfeeddead    # max_speed is too low, because we can get turbo.
        track = self.data_init['race']['track']['pieces']
        current_piece = track[current_piece_ind]
        lowest_radius = current_piece.get('radius', 0xfeeddead)
        lowest_opposite_radius = 0xfeeddead
        dist = self.piece_len(current_piece, lane) - current_piece_dist

        t = (track*2)[current_piece_ind+1:]
        cbs = True
        for p in t:
            if 'radius' in p and not('radius' in current_piece and self.same_direction(p, current_piece) and p['radius'] >= lowest_radius):
                speed = self.estimate_entry_speed(p, lane)
                d = dist
                while d > speed:
                    speed *= 1/self.accel
                    d -= speed

              #  if 'turboFactor' in self.turboParams and speed < self.speed*(2-(1-self.accel)*self.turboParams['turboFactor']):
               # if 'turboFactor' in self.turboParams \
               #     and speed < self.turboParams['turboFactor']*(self.max_speed) \
               #     *(1-math.pow(self.accel, self.turboParams['turboFactor'])) + speed * math.pow(self.accel, self.turboParams['turboFactor']) :
               #     cbs = False

                if speed < r_speed:
                    r_dist = dist
                    r_speed = speed
                    if self.same_direction(p, current_piece):
                        lowest_radius = p['radius']
                    else:
                        lowest_opposite_radius = p['radius']
            dist += self.piece_len(p, lane)
       # self.canBoostSafely = cbs
        return r_dist, r_speed


    
    def get_distance(self, index1, dist1, index2, dist2):
        if (index1 != index2):
            len_piece_1 = self.piece_len(self.data_init['race']['track']['pieces'][index1], self.lane)
            return dist2 + len_piece_1 - dist1
        else:
            return dist2 - dist1

    def is_piece_bend(self, index):
        return 'radius' in self.data_init['race']['track']['pieces'][index]
    
    def gather_info_about_enemies(self, data):
        # data is from CARPOSITIONS message                    
        for x in data:
            car_id = tuple(sorted(x['id'].items()))
            if car_id != self.car:
                info = {}
                # Gather info about single enemy
                info['inPieceDistance'] = x['piecePosition']['inPieceDistance']
                info['pieceIndex'] = x['piecePosition']['pieceIndex']
                info['angle'] = x['angle']
                info['lane'] = x['piecePosition']['lane']
                info['tick'] = self.tick
                info['car'] = x['id']
                
                isInitialized = True
                if (self.enemy[car_id][1] == 0):
                    isInitialized = False

                if (isInitialized):
                    start_idx = self.enemy[car_id][1]['pieceIndex']
                    end_idx = info['pieceIndex']
                    if (self.is_piece_bend(end_idx)):
                        info['radius'] = self.data_init['race']['track']['pieces'][end_idx]['radius']
                    else:
                        info['radius'] = 0

                    info['speed'] = self.get_distance(self.enemy[car_id][0]['pieceIndex'],
                                             self.enemy[car_id][0]['inPieceDistance'],
                                             info['pieceIndex'], info['inPieceDistance'])

                
                

                # Update history record
                self.enemy[car_id][1] = self.enemy[car_id][0]
                self.enemy[car_id][0] = info;

                
                # Check, if enemy changed from straight road to curve
                # If yes, then add note to waiting room
                if isInitialized and start_idx != end_idx and self.is_piece_bend(end_idx) and\
                not 'radius' in self.data_init['race']['track']['pieces'][start_idx]:
                    self.waRoom.append(info)
                
        

    def on_car_positions(self, data):
        self.tick += 1
        self.gather_info_about_enemies(data)
        
        
        (our_pos,) = (x for x in data if x['id'] == self.car)
        if self.lane is None:
            self.lane = our_pos['piecePosition']['lane']['endLaneIndex']

        if not self.lane:
            return self.switch_lane('Right')

        current_piece_ind = our_pos['piecePosition']['pieceIndex']
        current_piece_dist = our_pos['piecePosition']['inPieceDistance']
        if self.p_ind is None:
            self.p_ind = current_piece_ind
            self.ppos = current_piece_dist

        current_piece = self.data_init['race']['track']['pieces'][current_piece_ind]
        if not self.p_ind is current_piece_ind:
            print current_piece
            self.ppos -= self.piece_len(self.data_init['race']['track']['pieces'][current_piece_ind - 1], self.lane)
            self.p_ind = current_piece_ind

        angle = our_pos['angle']
        d_angle = angle - self.angle
        dd_angle = d_angle - self.d_angle
        ddd_angle = dd_angle - self.dd_angle
        dddd_angle = ddd_angle - self.ddd_angle
        self.angle = angle
        self.d_angle = d_angle
        self.dd_angle = dd_angle
        self.ddd_angle = ddd_angle

        speed = current_piece_dist - self.ppos
        accel = speed - self.speed

        if not self.accel and self.speed:
            self.accel = accel/self.speed
            self.max_speed = self.speed/(1-self.accel)
            print 'max speed', self.max_speed, 'accel', self.accel

        self.speed = speed
        self.ppos = current_piece_dist
        if not self.accel or not speed:
            return self.throttle(1)

        _, tempomat = self.next_piece_of_concern(current_piece_ind, current_piece_dist, self.lane)
        if 'angle' in current_piece:
            e_angle = angle
            ed_angle = d_angle
            edd_angle = dd_angle
            eddd_angle = ddd_angle
            while math.copysign(4, current_piece['angle']) * ed_angle > 0:
                if abs(e_angle) > 60:
                    tempomat = 0
                    break
                e_angle += ed_angle
                ed_angle += edd_angle
                edd_angle += ddd_angle
                eddd_angle += dddd_angle
            else:
                tempomat = min(tempomat, speed*60/abs(angle) if angle else self.max_speed)
        print tempomat, speed

        if not self.boosted:
            t = (tempomat - speed * self.accel)/(1-self.accel)/self.max_speed
        else:
            t = (tempomat - speed * self.accel)/(1-self.accel)/(self.max_speed)/self.turboParams['turboFactor']
            print('Wheee!!')
            
        if angle and 'radius' in current_piece:
            centrifugal = speed**2/current_piece['radius']
        else:
            centrifugal = 0
        print our_pos['piecePosition']['inPieceDistance'] , angle, d_angle, dd_angle, ddd_angle, dddd_angle, centrifugal, math.sin(math.radians(angle))

        if self.gotTurbo and 'length' in current_piece:
            self.canBoostSafely = True
            self.gotTurbo = False
            return self.throttle(0)

        if self.canBoostSafely:
            self.canBoostSafely = False
            return self.turbo()
            
        self.throttle(0 if t < 0 else 1 if t > 1 else t)

    def on_crash(self, data):
        print("Someone crashed")

    def on_game_end(self, data):
        print("Race ended")

    def on_error(self, data):
        print("Error: {0}".format(data))

    def on_game_init(self, data):
        self.data_init = data
        self.init_external_intelligence_module(data)
        print(data)

    def on_your_car(self, data):
        self.car = data

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_game_init,
            'turboAvailable': self.on_turbo_available,
            'turboEnd': self.on_turbo_end,
            'yourCar': self.on_your_car,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
