import json
import socket
import sys
import math

class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.angle = 0
        self.angle_max = 0
        self.d_angle = 0
        self.dd_angle = 0
        self.ddd_angle = 0
        self.ppos = 0
        self.speed = 0
        self.max_speed = 0
        self.accel = 0
        self.current_lane = None
        self.p_ind = None
        self.num_enemies = 0
        self.tick = 0
        self.laps_left = 2
        self.max_tempo_diff = 0
        self.force = .4
        self.force_min = 0
        self.centripetal = 0
        self.current_lap = -1
        self.last_crash_lap = -1
        self.crash_angle = 60
        self.insufficient_power = True

        self.measure_max_angle = False
        self.boosted = False
        self.gotTurbo = False
        self.turboParams = {}
        self.canBoostSafely = False
        self.ticksTillTurboEnd = 0
        self.boost_limit = 1
        self.enemy = {}

        self.ticksToKeep = 100
        self.waRoom = []
        self.knowledge = []

    def init_external_intelligence_module(self, data):
        # data is from GAMEINIT message
        for x in data['race']['cars']:
            car_id = tuple(sorted(x['id'].items()))
            if x['id'] != self.car:
                self.enemy[car_id] = {}
                self.enemy[car_id][0] = 0
                self.enemy[car_id][1] = 0
                self.num_enemies += 1

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data, "gameTick": self.tick}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name, "key": self.key})
##        return self.msg("joinRace", {"botId": {"name": self.name,
##                                 "key": self.key},
##                  #               "trackName": 'keimola',
##                                 "trackName": 'germany',
##                  #               "trackName": 'usa',
##                  #               "trackName": 'france',
##                                 "carCount": 1})

    def switch_lane(self):
        '''Returns True if switch was necessary, False otherwise.'''
        print 'current', self.current_lane, 'desired', self.desired_lane
        if self.current_lane == self.desired_lane:
            return False
        current_lane, = [l for l in self.lanes if l['index'] == self.current_lane]
        desired_lane, = [l for l in self.lanes if l['index'] == self.desired_lane]
        direction = "Right" if current_lane['distanceFromCenter'] - desired_lane['distanceFromCenter'] < 0 else "Left"
        self.msg("switchLane", direction)
        return True

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def turbo(self):
        #if self.gotTurbo:
        self.msg("turbo", 'AAA!')
        print 'N_2O'
        self.boosted = True
        self.gotTurbo = False
        self.ticksTillTurboEnd = self.turboParams['turboDurationTicks']

    def on_turbo_available(self, data):
        self.gotTurbo = True
        print 'got N_2O', data
        self.turboParams = data

    def on_turbo_end(self, data):
        print("Boost ended")
        #self.boosted = False

    def is_turbo_dead(self):
        # use only once per tick!
        if self.ticksTillTurboEnd > 0:
            self.ticksTillTurboEnd -= 1
        else:
            self.boosted = False

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")

    def on_game_start(self, data):
        self.throttle(1)
        print("Race started")

    def lane_radius(self, turn, lane):
        for l in self.lanes:
            if l['index'] == lane:
                break
        return turn['radius'] - math.copysign(1, turn['angle'])*l['distanceFromCenter']

    def piece_len(self, piece, lane):
        # It looks like {}.get(), but it doesn't compute the default value until it's necessary.
        return piece['length'] if 'length' in piece else math.radians(abs(piece['angle']))*self.lane_radius(piece, lane)

    def same_direction(self, a, b):
        '''Checks if turns a and b turn the same way.
            Returns false if one of the turns is in fact a straight.'''
        n = ('angle' in a) + ('angle' in b)
        if not n:
            return True
        if n == 1:
            return False
        return math.copysign(4, a['angle']) == math.copysign(4, b['angle'])

    def is_continuation(self, a ,b):
        '''Like same_direction, but additionally checks radii.
            Returns true if same_direction returns true
            and a is not tighter than b.'''
        return self.same_direction(a, b) and a['radius'] >= b['radius']

    def estimate_end_turn_ticks(self, current_piece_ind, current_piece_dist, lane, speed):
        current_piece = self.pieces[current_piece_ind]
        dist = self.piece_len(current_piece, lane) - current_piece_dist
        for p in (self.pieces*2)[current_piece_ind+1:]:
            if 'radius' in p and self.same_direction(p, current_piece) and p['radius'] == current_piece['radius']:
                dist += self.piece_len(p, lane)
            else:
                break
        return dist/speed

    def estimate_entry_speed(self, turn, lane, pieceIndex):
        maxSpeed = 0
        for fact in self.knowledge:
            if self.lane_radius(turn, fact['lane']) <= self.lane_radius(turn, lane) and fact['pieceIndex'] == pieceIndex:
                posSpeed = fact['speed']
                if ( (fact['angle'] <= self.angle+1 and self.pieces[pieceIndex]['angle'] > 0)\
                   or (fact['angle'] >= self.angle-1 and self.pieces[pieceIndex]['angle'] < 0) )\
                   and posSpeed > maxSpeed:
                    maxSpeed = posSpeed
        return max([math.sqrt(self.force*(self.lane_radius(turn, lane))), maxSpeed])

    def next_piece_of_concern(self, current_piece_ind, current_piece_dist, lane, laps_left):
        '''Returns the distance to the piece we should consider next
            as well as estimated speed we shouldn't exceed while approaching it.
            If there's a switch piece on the way, the third return value is True.

            WARNING: Don't call until self.accel is known!'''
        r_dist = 0
        r_speed = 0xfeeddead    # max_speed is too low, because we can get turbo.
        r_switch = False
        switch = False
        current_piece = self.pieces[current_piece_ind]
        lowest_radius = current_piece.get('radius', 0xfeeddead)
        lowest_opposite_radius = 0xfeeddead
        dist = self.piece_len(current_piece, lane) - current_piece_dist

        for p in (self.pieces*min(2, laps_left))[current_piece_ind+1:]:
            switch = switch or p.get('switch', False)
            if 'radius' in p and not('radius' in current_piece and self.same_direction(p, current_piece) and p['radius'] >= lowest_radius):
                speed = self.estimate_entry_speed(p, lane, current_piece_ind)
                d = dist
                while d > speed:
                    speed /= self.accel
                    d -= speed
                speed /= self.accel**(d/speed)
                if speed < r_speed:
                    r_dist = dist
                    r_speed = speed
                    r_switch = r_switch or switch
                    if self.same_direction(p, current_piece):
                        lowest_radius = p['radius']
                    else:
                        lowest_opposite_radius = p['radius']
            dist += self.piece_len(p, lane)

        return r_dist, r_speed, r_switch

    def get_distance(self, index1, dist1, index2, dist2, lane):
        if (index1 != index2):
            len_piece_1 = self.piece_len(self.pieces[index1], lane)
            return dist2 + len_piece_1 - dist1
        else:
            return dist2 - dist1

    def is_piece_bend(self, index):
        return 'radius' in self.pieces[index]

    def gather_info_about_enemies(self, data):
        # data is from CARPOSITIONS message
        for x in data:
            car_id = tuple(sorted(x['id'].items()))
            if x['id'] != self.car:
                info = {}
                # Gather info about single enemy
                info['inPieceDistance'] = x['piecePosition']['inPieceDistance']
                info['pieceIndex'] = x['piecePosition']['pieceIndex']
                info['angle'] = x['angle']
                info['lane'] = x['piecePosition']['lane']['endLaneIndex']
                info['tick'] = self.tick
                info['car'] = car_id

                isInitialized = True
                if (self.enemy[car_id][1] == 0):
                    isInitialized = False

                if (isInitialized):
                    start_idx = self.enemy[car_id][1]['pieceIndex']
                    end_idx = info['pieceIndex']
                    if (self.is_piece_bend(end_idx)):
                        info['radius'] = self.pieces[end_idx]['radius']
                    else:
                        info['radius'] = 0

                    info['speed'] = self.get_distance(self.enemy[car_id][0]['pieceIndex'],
                                             self.enemy[car_id][0]['inPieceDistance'],
                                             info['pieceIndex'], info['inPieceDistance'],
                                             info['lane'])

                # Update history record
                self.enemy[car_id][1] = self.enemy[car_id][0]
                self.enemy[car_id][0] = info;

                # Check, if enemy changed from straight road to curve
                # If yes, then add note to waiting room
                if isInitialized and not self.same_direction(self.pieces[start_idx], self.pieces[end_idx]) and\
                    'radius' in self.pieces[end_idx]:
                    new_info = self.enemy[car_id][1]
                    new_info['angle'] = info['angle']
                    new_info['radius'] = info['radius']
                    new_info['pieceIndex'] = end_idx
                    self.waRoom.append(new_info)

    def estimate_lane_time(self, lane):
        switched = False
        dist = self.piece_len(self.pieces[self.p_ind], self.current_lane) - self.ppos
        obstacles = {}

        for i, p in list(enumerate(self.pieces*min(2, self.laps_left)))[self.p_ind+1:]:
            if switched:
                if p.get('switch', False):
                    break
                for c in obstacles:
                    obstacles[c] += self.piece_len(p, lane)
                for cid, hist in self.enemy.items():
                    if hist[0]['lane'] == lane and hist[0]['pieceIndex'] == i % len(self.pieces):
                        obstacles[cid] = self.piece_len(p, lane) - hist[0]['inPieceDistance']
                dist += self.piece_len(p, lane)
            else:
                switched = p.get('switch', False)
                if switched:
                    for cid, hist in self.enemy.items():
                        if hist[0]['lane'] == lane and hist[0]['pieceIndex'] == i % len(self.pieces):
                            obstacles[cid] = self.piece_len(p, lane) - hist[0]['inPieceDistance']
                dist += self.piece_len(p, self.current_lane)
        print 'lane', lane, 'dist', dist
        return max([dist/(self.speed+.2)] + [dist/(self.enemy[cid][0].get('speed', 0)+.1) for cid, dist in obstacles.items()])

    def update_desired_lane(self):
        '''Returns advised speed of approaching the next dangerous curve on currently preferred lane.'''
        for _, l in sorted((self.estimate_lane_time(l['index']), l['index']) for l in self.lanes if abs(self.current_lane - l['index']) <= 1):
            print 'lane', l, 'time', _
            _, tempomat, _ = self.next_piece_of_concern(self.p_ind, self.ppos, l, self.laps_left)
            print tempomat, self.speed
            if self.speed * self.accel <= tempomat + .001:
                break
        self.desired_lane = l
        print 'desired lane', l
        return tempomat

    def on_car_positions(self, data):
        self.gather_info_about_enemies(data)

        (our_pos,) = (x for x in data if x['id'] == self.car)
        self.current_lane = our_pos['piecePosition']['lane']['endLaneIndex']
        self.laps_left = self.data_init['race']['raceSession'].get('laps', 0xfeeddead) - our_pos['piecePosition']['lap']

        current_piece_ind = our_pos['piecePosition']['pieceIndex']
        current_piece_dist = our_pos['piecePosition']['inPieceDistance']
        if self.p_ind is None:
            self.p_ind = current_piece_ind
            self.ppos = current_piece_dist

        current_piece = self.pieces[current_piece_ind]
        previous_piece = self.pieces[(current_piece_ind - 1)%len(self.pieces)]
        if 'radius' in current_piece and not self.is_continuation(current_piece, previous_piece):
            self.measure_max_angle = True
        next_piece = self.pieces[(current_piece_ind + 1)%len(self.pieces)]
        if not self.p_ind is current_piece_ind:
            print current_piece
            if previous_piece.get('switch', False) or 'radius' in previous_piece:
                self.ppos = current_piece_dist - self.speed
            else:
                self.ppos -= self.piece_len(previous_piece, self.current_lane)
            self.p_ind = current_piece_ind

        if our_pos['piecePosition']['lap'] != self.current_lap:
            d = self.force - self.force_min
            self.current_lap = our_pos['piecePosition']['lap']
            if self.current_lap - self.last_crash_lap > 1 and not self.insufficient_power:
                print 'clear lap', self.force_min, self.centripetal, self.force
                t = min(1, max(self.crash_angle/2., self.angle_max) / (self.crash_angle*.95))
#                self.force -= self.centripetal
#                self.force_min -= self.centripetal
                self.force_min = self.force * .9
                self.force /= t**(1-t)
#                self.force += self.centripetal
#                self.force_min += self.centripetal
            else:
                self.last_crash_lap = min(self.last_crash_lap, self.current_lap)
            self.insufficient_power = True
            print 'new lap', self.angle_max, 'force', self.force
        angle = our_pos['angle']
        if self.measure_max_angle:
            self.angle_max = max(self.angle_max, angle*math.copysign(1, current_piece['angle']) if 'angle' in current_piece else 1)
        self.crash_angle = max(self.crash_angle, self.angle_max)
        d_angle = angle - self.angle
        dd_angle = d_angle - self.d_angle
        ddd_angle = dd_angle - self.dd_angle
        dddd_angle = ddd_angle - self.ddd_angle
        self.angle = angle
        self.d_angle = d_angle
        self.dd_angle = dd_angle
        self.ddd_angle = ddd_angle

        speed = abs(current_piece_dist - self.ppos)
        accel = speed - self.speed

        if not self.accel and self.speed:
            self.accel = accel/self.speed
            self.max_speed = self.speed/(1-self.accel)
            print 'max speed', self.max_speed, 'accel', self.accel

        self.speed = speed
        self.ppos = current_piece_dist
        if not self.accel or not speed:
            return self.throttle(1)

        self.is_turbo_dead()
        self.keep_some_facts()

        dist, tempomat, switch_avail = self.next_piece_of_concern(current_piece_ind, current_piece_dist, self.current_lane, self.laps_left)
        if not self.boosted:
            est_speed = speed
            while dist > 0:
                est_speed/=self.accel
                dist -= est_speed
            self.boost_limit = est_speed
        else:
            if tempomat == 0xfeeddead:
                return self.throttle(1)
            est_speed = self.boost_limit
            while dist > 0:
                est_speed/=self.accel
                dist -= est_speed
            tempomat = min(tempomat, est_speed)
        if switch_avail:
            tempomat = self.update_desired_lane()
            if next_piece.get('switch', False):
                print 'next piece is a switch. current_piece_dist', current_piece_dist, 'piece len', self.piece_len(current_piece, self.current_lane), 'speed', speed
            if next_piece.get('switch', False) and current_piece_dist + speed/self.accel > self.piece_len(current_piece, self.current_lane):
                print 'switching lane'
                if self.switch_lane():
                    print 'switch successful'
                    return

        if 'angle' in current_piece:
            mangle = abs(angle)
            mdangle = abs(d_angle)
            e_angle = angle
            ed_angle = d_angle
            edd_angle = dd_angle
            eddd_angle = ddd_angle
            if 'angle' not in previous_piece and current_piece_dist <3*speed and not self.insufficient_power:
                return self.throttle(0)
            t = self.estimate_end_turn_ticks(current_piece_ind, current_piece_dist, self.current_lane, speed)
            while t > -4*max(1,d_angle**2) and current_piece['angle'] * ed_angle > 0:
                mangle = max(mangle, abs(e_angle))
                mdangle = max(mdangle, abs(ed_angle))
                if mangle/self.accel > self.crash_angle - mdangle**2:
                    t=min(max(1,mdangle**2),t)
                e_angle += ed_angle
                ed_angle += edd_angle
                if self.measure_max_angle and current_piece['angle'] * edd_angle > 0:
                    edd_angle += eddd_angle
#                eddd_angle += dddd_angle
                t -= 1
            if mangle:
                t = (self.crash_angle-1)/mangle
                r = self.lane_radius(current_piece, self.current_lane)
                centrifugal = speed**2/r
            #    print t, mangle, r, centrifugal
                centrifugal *= t#**(1-t) #if t**(1-t) else 2
            tempomat = min(tempomat, (centrifugal * r)**.5 if angle else self.max_speed*self.turboParams['turboFactor'] if self.boosted else self.max_speed)
            if self.boosted:
                tempomat = min(tempomat, speed) if current_piece['angle'] * ed_angle < 0 else 0

        if tempomat > speed * 1/self.accel:
            if self.measure_max_angle:
                print angle
            self.measure_max_angle = False

        if not self.boosted:
            t = (tempomat - speed * self.accel)/(1-self.accel)/self.max_speed
        else:
            t = (tempomat - speed * self.accel)/(1-self.accel)/(self.max_speed)/self.turboParams['turboFactor']
            print('Wheee!!', tempomat, speed, t, self.max_tempo_diff)
            print 'radius' in current_piece, tempomat, speed, angle, d_angle, dd_angle, ddd_angle, dddd_angle, math.sin(math.radians(angle))

        if 'radius' in current_piece:
            centrifugal = speed**2/self.lane_radius(current_piece, self.current_lane)
            if not angle and self.ppos > speed * 2:
                self.centripetal = max(self.centripetal, centrifugal)
                if self.force_min < centrifugal:
                    self.force_min = centrifugal
                    self.centripetal = centrifugal
                    self.force = max(self.force, centrifugal*(2-self.accel))
        else:
            centrifugal = 0
        print our_pos['piecePosition']['inPieceDistance'] , angle, d_angle, dd_angle, ddd_angle, dddd_angle, centrifugal, math.sin(math.radians(angle))

        if 'length' in current_piece and 'angle' in previous_piece:
            if self.current_lap > self.last_crash_lap + 1:
                self.max_tempo_diff = max(self.max_tempo_diff, tempomat - speed)
            if self.gotTurbo and tempomat - speed > .9 * self.max_tempo_diff:
                self.gotTurbo = False
                return self.turbo()

        self.insufficient_power &= 'radius' in current_piece or t >= 1

        self.throttle(0 if t < 0 else 1 if t > 1 else t)

    def on_crash(self, data):
        if data == self.car:
            self.max_tempo_diff /= self.force
            self.force = (self.force + self.force_min)/2.
            self.max_tempo_diff *= self.force
            self.angle_max = 0
            self.last_crash_lap = self.current_lap
            print("We crashed! force", self.force)
        else:
            print("Someone else crashed.")
            print data
            self.waRoom = [fact for fact in self.waRoom \
                       if fact['car'] != tuple(sorted(data.items()))]

    def keep_some_facts(self):
        know = [fact for fact in self.waRoom \
                       if self.tick - fact['tick'] > self.ticksToKeep]
        self.waRoom = [fact for fact in self.waRoom \
                       if self.tick - fact['tick'] <= self.ticksToKeep]
        self.knowledge.extend(know)

    def on_game_end(self, data):
        print("Race ended")

    def on_error(self, data):
        print("Error: {0}".format(data))

    def on_game_init(self, data):
        self.data_init = data
        self.pieces = data['race']['track']['pieces']
        self.lanes = data['race']['track']['lanes']
        self.init_external_intelligence_module(data)
        print(data)

    def on_your_car(self, data):
        self.car = data

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_game_init,
            'turboAvailable': self.on_turbo_available,
            'turboEnd': self.on_turbo_end,
            'yourCar': self.on_your_car,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data, self.tick = msg['msgType'], msg['data'], msg.get('gameTick', 0)
            if msg_type in msg_map:
                msg_map[msg_type](data)
            print("Got {0} with tick {1}.".format(msg_type, msg.get('gameTick', None)))
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
