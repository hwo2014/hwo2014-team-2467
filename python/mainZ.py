import json
import socket
import sys
import math

class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.angle = 0
        self.d_angle = 0
        self.dd_angle = 0
        self.ddd_angle = 0
        self.ppos = 0
        self.speed = 0
        self.max_speed = 0
        self.accel = 0
        self.lane = None

        self.max_angle = 0.0
     
        self.track = []
        self.boosted = False
        self.gotTurbo = False
        self.turboParams = {}

        self.flag = False

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("joinRace", {"botId": {"name": self.name,
                                 "key": self.key},
                                 "trackName": 'germany',
                  #               "trackName": 'usa',
                  #               "trackName": 'france',
                                 "carCount": 1})

    def switch_lane(self, direction):
        self.lane += 1 if direction == 'Right' else -1
        return self.msg("switchLane", direction)

    def throttle(self, throttle):
        self.msg("throttle", throttle)
        
    def turbo(self):
        self.msg("turbo", 'AAA!')
        
    def on_turbo_end(self, data):
        print("Boost ended")
        self.boosted = False
        
    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")

    def on_game_start(self, data):
        print("Race started")

    def on_car_positions(self, data):
        if self.lane is None:
            self.lane = data[0]['piecePosition']['lane']['endLaneIndex']
        if not self.lane: #data[0]['piecePosition']['lane']['endLaneIndex'] != 2:
            self.switch_lane('Right')

            
        current_piece = self.data_init['race']['track']['pieces'][data[0]['piecePosition']['pieceIndex']]

        if 'radius' in current_piece and data[0]['angle']:
            print('ANGLE!')
            print(current_piece['radius'])
            print(data[0]['angle'])
            print(self.speed)
            #self.flag = True

        angle = data[0]['angle']
        d_angle = angle - self.angle
        dd_angle = d_angle - self.d_angle
        ddd_angle = dd_angle - self.dd_angle
        self.angle = angle
        self.d_angle = d_angle
        self.dd_angle = dd_angle
        

        if angle > self.max_angle:
            self.max_angle = angle

        speed = data[0]['piecePosition']['inPieceDistance'] - self.ppos
        if data[0]['piecePosition']['inPieceDistance'] < self.ppos:
            speed = self.speed #trzeba dokladniej
            accel = self.accel
        else:
            accel = speed - self.speed

        if speed < 0:
            print(current_piece)

        if not self.accel and self.speed:
            self.accel = accel/self.speed
            self.max_speed = self.speed/(1-self.accel)
            print('max speed', self.max_speed, 'accel', self.accel)

        self.speed = speed
        self.ppos = data[0]['piecePosition']['inPieceDistance']
        if not self.accel:
            return self.throttle(1)

        tempomat = 4.9
        t = (tempomat - speed * self.accel)/(1-self.accel)/self.max_speed
        if angle and 'radius' in current_piece:
            centrifugal = speed**2/current_piece['radius']
            print(angle, d_angle, dd_angle, ddd_angle, centrifugal, math.sin(math.radians(angle)))

        self.throttle(0 if t < 0 else 1 if t > 1 else t)
       
### CONRY ZONE
       ## http://i.imgur.com/MWPMz.jpg
       
##        pieceIndex = data[0]['piecePosition']['pieceIndex']
##        throttle = 1
##        if 'length' in self.track[pieceIndex]:
##            summed_distance = 0.01 + self.track[pieceIndex]['length'] - self.ppos
##        else:
##            summed_distance = 0.01 + self.track[pieceIndex]['radius'] \
##                                   * math.radians(self.track[pieceIndex]['angle']) \
##                                   - self.ppos
##        maxthrottle = 1.0
##
##        for i in range(1,10):
##            if 'length' in self.track[(pieceIndex+i)%len(self.track)]:
##                summed_distance += self.track[(pieceIndex+i)%len(self.track)]['length']
##            if 'radius' in self.track[(pieceIndex+i)%len(self.track)]:
##
##                # the new generation of pseudo-science computation
##                ticksToThis = summed_distance / (speed + 0.01)
##               # if ticksToThis > 3:
##               #     ticksToThis = 3
##                tempomat = 1
##                pseudoSpeed = speed
##                counter = -1
##                t = -1
##                while t < 0:    
##                    t = (tempomat - pseudoSpeed * self.accel)/(1-self.accel)/self.max_speed
##                    pseudoSpeed = tempomat / self.accel
##                    counter += 1
##
##                if counter >= ticksToThis  :
##                    throttle = 0.0
                
##                tempThrottle = 0.0
##
##                # wtf is that
##                if math.pow(9/(self.speed+0.01), 1/(80*summed_distance)) < 0.9999: # 0.04 for germany
##                    throttle = 0.0

                
##                summed_distance += self.track[(pieceIndex+i)%len(self.track)]['radius'] \
##                                   * math.radians(self.track[(pieceIndex+i)%len(self.track)]['angle'])
##        self.throttle(throttle)
###

                    
    def on_crash(self, data):
        print("Someone crashed")

    def on_game_end(self, data):
        print("Race ended")
        print(self.max_angle)

    def on_error(self, data):
        print("Error: {0}".format(data))

    def on_game_init(self, data):
        self.data_init = data
        print(data)
        
        for piece in data['race']['track']['pieces']:
            self.track.append(piece)

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_game_init,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                if msg_type == 'turboAvailable':
                    self.gotTurbo = True
                    self.turboParams = data
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()








##import json
##import socket
##import sys
##import math
##
##
##class NoobBot(object):
##
##    def __init__(self, socket, name, key):
##        self.socket = socket
##        self.name = name
##        self.key = key
##        self.lastPieceIndex = -1
##        self.track = []
##        self.speed = 0.0
##        self.acceleration = 0.0
##
##        self.lastPosition = 0
##
##        self.boosted = False
##        self.gotTurbo = False
##        self.turboParams = {}
##
##    def msg(self, msg_type, data):
##        self.send(json.dumps({"msgType": msg_type, "data": data}))
##
##    def send(self, msg):
##        self.socket.sendall(msg + "\n")
##
##    def join(self):
##        return self.msg("join", {"name": self.name,
##                                 "key": self.key})
####        return self.msg("joinRace", {"botId":{"name": self.name,
####                                 "key": self.key}, "trackName":"keimola"})
##
##    def throttle(self, throttle):
##        self.msg("throttle", throttle)
##        
##    def turbo(self):
##        self.msg("turbo", 'AAA!')
##        
##    def on_turbo_end(self, data):
##        print("Boost ended")
##        self.boosted = False
##        
##    def ping(self):
##        self.msg("ping", {})
##
##    def run(self):
##        self.join()
##        self.msg_loop()
##
##    def on_join(self, data):
##        print("Joined")
##        self.ping()
##
##    def on_game_init(self, data):
##        print("Race initializing")
##        self.ping()
##
##        for piece in data['race']['track']['pieces']:
##            self.track.append(piece)
##
##        # ustalanie maxspeed dla zakretow
##            
##        
##    def on_game_start(self, data):
##        print("Race started")
##        self.ping()
##
##    def on_car_positions(self, data):
##        if data[0]['piecePosition']['pieceIndex'] != self.lastPieceIndex:
##            self.lastPieceIndex = data[0]['piecePosition']['pieceIndex']
##            print(self.lastPieceIndex)
##            print("Speed: ", self.speed)
##            print("Acceleration: ", self.acceleration)
##        else:
##            if self.speed == 0:
##                self.speed = 0.0001
##            self.acceleration = (data[0]['piecePosition']['inPieceDistance'] - self.lastPosition) / self.speed
##            self.speed = data[0]['piecePosition']['inPieceDistance'] - self.lastPosition
##            
##        self.lastPosition = data[0]['piecePosition']['inPieceDistance']
##            
##     # ponizej sa brzydkie glupoty, przepraszam   
##
##        throttle = 1
##        summed_distance = 0.01
##        maxthrottle = 1.0
##
##        for i in range(1,10):
##            if 'length' in self.track[(self.lastPieceIndex+i)%len(self.track)]:
##                summed_distance += self.track[(self.lastPieceIndex+i)%len(self.track)]['length']
##            if 'radius' in self.track[(self.lastPieceIndex+i)%len(self.track)]:
##                if math.pow(0.3/(self.speed+0.01), 1/(80*summed_distance)) < 0.022: # 0.04 for germany
##                    throttle = 0.0
##                    #print(math.pow(0.3/(self.speed+0.01), 1/(80*summed_distance)))
##            
##
##        ## bo to turbo to tak na niby        
##        if self.boosted:
##            throttle /= self.turboParams['turboFactor']
##
##        self.throttle(throttle)
##
##            
##
##    def on_crash(self, data):
##        print("Someone crashed")
##        self.ping()
##
##    def on_game_end(self, data):
##        print("Race ended")
##        self.ping()
##
##    def on_error(self, data):
##        print("Error: {0}".format(data))
##        self.ping()
##
##    def msg_loop(self):
##        msg_map = {
##            'join': self.on_join,
##            'gameInit': self.on_game_init,
##            'gameStart': self.on_game_start,
##            'carPositions': self.on_car_positions,
##            'crash': self.on_crash,
##            'gameEnd': self.on_game_end,
##            'error': self.on_error,
##            'turboEnd': self.on_turbo_end,
##        }
##        socket_file = s.makefile()
##        line = socket_file.readline()
##        while line:
##            msg = json.loads(line)
##            msg_type, data = msg['msgType'], msg['data']
##            if msg_type in msg_map:
##                msg_map[msg_type](data)
##            else:
##                print("Got {0}".format(msg_type))
##                if msg_type == 'turboAvailable':
##                    self.gotTurbo = True
##                    self.turboParams = data
##                self.ping()
##            line = socket_file.readline()
##
##
##if __name__ == "__main__":
##    if len(sys.argv) != 5:
##        print("Usage: ./run host port botname botkey")
##    else:
##        host, port, name, key = sys.argv[1:5]
##        print("Connecting with parameters:")
##        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
##        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
##        s.connect((host, int(port)))
##        bot = NoobBot(s, name, key)
##        bot.run()
